#!/usr/bin/env python3

import psutil
import os
import sys
import getopt
import smtplib

global conf
conf = {'cpu': '80', 'mem': '80', 'disk': '80'}         # Default values for alarming

def usage():                                            # Function to print out the usage
    print ('\n------------------------------------------------------------\n')
    print ('Usage:\ths.mon.py PARANETER [Option]\n\ths_mon.py --disk="/"')
    print ('Parameters:\n\t-c   --cpu=\tReturn CPU monitoring data\n\t-m   --memory=\tReturn Memory monitoring data'
           '\n\t-d   --disk=\tReturn Disk monitoring data\n\t-a   --all\tReturn all monitoring data (Default)'
           '\n\t-C   --check\tCompares the monitoring data with the in mon.ini limits'
           '\n\t     --check\tChanging the values defined in mon.ini')
    print ('Options:\n\t'
           '--cpu=     all | load | count | percent\n\t'
           '--memory=  all | gb | percent | total\n\t'
           '--disk=    all | <Path>\n\t'
           '--check=   all | cpu | mem | disk\n\t'
           '--change=  cpu=<Value> | mem=<Value> | disk=<Value>\n\t'
           '\n\tNote: all is always default!')
    print ('\n------------------------------------------------------------\n')


def gb(byte):                    # Function for Byte - GigaByte conversion
    for i in range(3):
        byte = byte / 1024
    return byte


def cpu(x):                                     # CPU Calcolations
    percent = psutil.cpu_percent(interval=1)
    lavg = os.getloadavg()
    count = psutil.cpu_count()
    if x in [ 'percent' ]:
        print ('CPU usage = ' + str(percent) + '%')
    elif x in [ 'count' ]:
        print ('CPU count = ' + str(count))
    elif x in [ 'load' ]:
        print ('Load avarage = ' + str(lavg))
    elif x in [ 'C' ]:
        return percent
    elif x in [ 'all' ]:
        print ('CPU usage = ' + str(percent) + '%')
        print ('CPU count = ' + str(count))
        print ('Load avarage = ' + str(lavg))
    else:
        usage()


def mem(x):                                     #Memory calculations
    total = psutil.virtual_memory().total
    free = psutil.virtual_memory().available
    used = total - free                         # The .used function of psutil seems to give wrong numbers. This way is the same as htop
    if x in [ 'gb' ]:
        print ('Memory usage in GB = {0:.2f}GB / {1:.2f}GB'.format(gb(used), gb(total)))
    elif x in [ 'percent' ]:
        print ('Memory usage in % = {0:.2f}%'.format(psutil.virtual_memory().percent))
    elif x in [ 'C' ]:
        return psutil.virtual_memory().percent
    elif x in [ 'all' ]:
        print ('Memory usage in GB = {0:.2f}GB / {1:.2f}GB'.format(gb(used), gb(total)))
        print ('Memory usage in % = {0:.2f}%'.format(psutil.virtual_memory().percent))
    elif x in [ 'total' ]:
        print ('Total Memory in GB = {0:.2f}GB'.format(gb(total)))
    else:
        usage()


def disk(x):                                        #Disk calculations
    dinput = psutil.disk_partitions()
    dlist = str.split(str(dinput), "'")
    dpath = [x for x in dlist if "/dev" in x]       #get the Path of all mounted Disks
    var = {}
    if x in [ 'all' ]:
        for i in dpath:
            mount = os.popen("mount |grep "+ i).readlines()             #For psutils the mountpoint is needed. /dev/sda1 would give wrong numbers
            dir = str.split(str(mount), ' ')[2]
            print('Disk usage of {0} = {1:.2f}GB / {2:.2f}GB'.format(dir, gb(psutil.disk_usage(dir).used), gb(psutil.disk_usage(dir).total)))
    elif x in [ 'C' ]:
        for i in dpath:
            mount = os.popen("mount |grep "+ i).readlines()
            dir = str.split(str(mount), ' ')[2]
            k = psutil.disk_usage(dir).percent
            var = {i: int(k)}
            return var
    else:
        if '/dev/' in x:                                            #checks if the input ist mountpoint or path
            mount = os.popen("mount |grep " + x).readlines()
            dir = str.split(str(mount), ' ')[2]
            x = dir
        print('Disk usage of {0} = {1:.2f}GB / {2:.2f}GB'.format(x, gb(psutil.disk_usage(x).used), gb(psutil.disk_usage(x).total)))


def ini():                                               #Function to Create and / or Read the .ini-file
    try:
        ini_file = open('mon.ini', 'r')                  #Reads the file when it exist
        line = ini_file.readlines()[1:]
        for x in line:
            split = str.split(x, ' ')
            conf[split[0]] = split[2].rstrip()
        ini_file.close()
        return conf
    except IOError as error2:
        print (error2)
        ini_file = open('mon.ini', 'w')                 #Writes the file when not
        print('\n\tmon.ini created\n')
        ini_file.write('[maximum_in_percent]\n')
        for x in conf:
            ini_file.write('{0} = {1}\n'.format(str.split(str(x), ' ')[0], conf[x]))
        ini_file.close()
        return conf


def change(x):                              #function to change the .ini file
    try:
        var = str.split(str(x), '=')        #input has to be <part>=<newvalue> and will be splited here
        conf[var[0]] = var[1]
        ini_file = open('mon.ini', 'w')
        ini_file.write('[maximum_in_percent]\n')
        for i in conf:                              #The new Values will be written to the file
            ini_file.write('{0} = {1}\n'.format(str.split(str(i), ' ')[0], conf[i]))
        ini_file.close()
    except ValueError as error3:
        print(error3)


def alert(part, value, max):                                # Example of an E-Mail function
    server = smtplib.SMTP('smtp.gmail.com', 587)            # i would prefer to use exim or some thing similar
    server.login("youremailusername", "password")           # and not rely on exteral mail server
    msg = "\nMonitoring Alert!\n{} is at {}% the limit was set to {}".format(part, value, max)
    server.sendmail("you@gmail.com", "target@example.com", msg)      # and you shouldn't use this function
                                                                     # always hide your Password :-p

def compare(mon, test):                                 # This function compares the max values from the .ini file
    if not type(test) is dict:                          # with the actual usage and alerts if the limit is reached.
        var = int(conf[mon])
        if test >= var:
            print('ALARM!!!\t{} is to high!!!'.format(mon))
            #alert(mon, test, var)
    else:                                               #The Disk Function returns a Dictonary, here must be checked if "test" is a dict or not
        for var in test:
            if test[var] >= int(conf[mon]):
                print('ALARM!!!\t{} is to high!!!'.format(mon))
                #alert(mon, var, int(conf[mon]))

def check(function):                           # This function starts the compare process
    if function in [ 'all' ]:
        compare('cpu', cpu('C'))
        compare('mem', mem('C'))
        compare('disk', disk('C'))
    elif function in [ 'cpu' ]:
        compare('cpu', cpu('C'))
    elif function in [ 'mem' ]:
        compare('mem', mem('C'))
    elif function in [ 'disk' ]:
        compare('disk', disk('C'))
    else:
        usage()


### Main-part ###

argv=sys.argv[1:]               # Check for Comandline Arguments
ini()                           # run the ini function to create or read the .ini file
try:
    opts, args = getopt.getopt(argv, 'cmdaC', ['cpu=', 'memory=', 'disk=', 'all=', 'check=', 'change=' ])  # declare all allowed arguments
except getopt.GetoptError as error1:
    print (error1)
    usage()
    sys.exit()
for opt, arg in opts:                       # Action depending on argument.
    if arg is '':
        arg = 'all'                         # Sets "all" as default
    if opt in [ '-c', '--cpu' ]:
        cpu(arg)
    elif opt in [ '-m', '--memory' ]:
        mem(arg)
    elif opt in [ '-d', '--disk' ]:
        disk(arg)
    elif opt in [ '-C', '--check' ]:
        check(arg)
    elif opt in [ '--change' ]:
        change(arg)
    elif opt in [ '-a', '--all' ]:
        cpu(arg)
        mem(arg)
        disk(arg)
