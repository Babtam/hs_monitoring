# HS_Monitoring - README

HS_Monitorin is used to monitor CPUs, memory and hard disks.

The limits are saved in a file called mon.ini.
You can change the limits by editing the file or using the `` --change `` parameter.

## Usage:

	hs.mon.py PARANETER [Option]
	hs_mon.py --disk="/"')

#### Parameters:

	-c		--cpu=			Return CPU monitoring data
	-m		--memory=		Return Memory monitoring data
	-d		--disk=			Return Disk monitoring data
	-a		--all=			Return all monitoring data (default)
	-C		--check=		Compares the monitoring data with
										the limits saved in mon.ini
			--change		Changing the values defined in mon.ini

#### Options:

	--cpu=     all | load | count | percent
	--memory=  all | gb | percent | total
	--disk=    all | <Path>
	--check=   all | cpu | mem | disk
	--change=  cpu=<Value> | mem=<Value> | disk=<Value>

	Note: "all" is always default option!


## How To

While the main function `of hs_monitoring.py` is to check cpu usage, memory usage and disk space, it also provides useful hardware data.

You can do this e.g. with the `-c` parameter, here you will get all CPU data.

`./hs_mon.py -c
CPU usage = 8.3%
CPU count = 4
Load avarage = (0.48, 0.61, 0.54)`

If you only want to have a certain value e.g. the load average this can be done via the `--cpu=` parameter and the above mentioned options.

`./hs_mon.py --cpu=load
Load avarage = (0.55, 0.59, 0.55)`

The actual check of all hardware components works with the parameter `-C`.
To specify a component you can use `--check=`.
Only the specified components will then be checked.

`./hs_mon.py -C
ALARM!!!	cpu is to high!!!
ALARM!!!	mem is to high!!!
ALARM!!!	disk is to high!!!`

`./hs_mon.py --check=cpu
ALARM!!!	cpu is to high!!!`

You can change the monitoring limits using the `--change` function.
The function writes the new values in mon.ini to save them permanently.

`./hs_mon.py --change cpu=60
./hs_mon.py --change mem=75
./hs_mon.py --change disk=90`

It is recommended to use hs_monitoring.py as cronjob, as this ensures permanent monitoring at regular intervals.

Mail alerting is deactivated in this version, but can be reactivated at any time by adding mail account data.

I recommend to use exim or another local mail server and integrate it into the script.

### Sources:		
[Python Documentation](https://docs.python.org/3.5/),
[getopt Documentation](https://docs.python.org/3.0/library/getopt.html),
[psutil Documentation](https://psutil.readthedocs.io/en/latest/) and    											https://pypi.python.org/pypi/psutil
